package bg.tilchev.springangularcrud;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import bg.tilchev.springangularcrud.entities.User;
import bg.tilchev.springangularcrud.repositories.UserRepository;
import bg.tilchev.springangularcrud.services.UserServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceImplTests {

    @Autowired
    private UserServiceImpl userService;
    
    @MockBean
    private UserRepository userRepoMock;
    
    @Test
    public void testCreateShouldCallUserRepoSaveAndFlush() {
        // arrange
        User expectedUser = new User();
        expectedUser.setEmail("ppeshov@accenture.com");
        expectedUser.setFirstName("Pesho");
        expectedUser.setLastName("Peshov");
        expectedUser.setId(1L);
        User inputUser = new User();
        inputUser.setEmail("ppeshov@accenture.com");
        inputUser.setFirstName("Pesho");
        inputUser.setLastName("Peshov");
        Mockito.when(this.userRepoMock.saveAndFlush(inputUser))
            .thenReturn(expectedUser);
        
        // act
        User actualUser = this.userService.create(inputUser);
        
        // assert
        Mockito.verify(this.userRepoMock, Mockito.times(1))
            .saveAndFlush(inputUser);
        Assert.assertEquals(expectedUser, actualUser);
    }
    
    
   @Test
   public void testGetAllUsersThenReturnAllUsers() {
       User expectedUser = new User();
       expectedUser.setEmail("ppeshov@accenture.com");
       expectedUser.setFirstName("Pesho");
       expectedUser.setLastName("Peshov");
       expectedUser.setId(1L);
       User inputUser = new User();
       inputUser.setId(2L);
       inputUser.setEmail("ppeshov@accenture.com");
       inputUser.setFirstName("Pesho");
       inputUser.setLastName("Peshov");
       
      List<User> users = new ArrayList<>();
      users.add(expectedUser);
      users.add(inputUser);
      
      Mockito.when(this.userRepoMock.findAll())
      .thenReturn(users);
      
      List<User> userss = userService.findAll();
      
      Mockito.verify(this.userRepoMock, Mockito.times(1)).findAll();
      
      Assert.assertArrayEquals(userss.toArray(), users.toArray());
      
   }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
