package bg.tilchev.springangularcrud.interceptors;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 *
 * @author todor.ilchev
 */
public class ExecuteTimeInterceptor extends HandlerInterceptorAdapter {
    
    private static final Logger logger = Logger.getLogger(ExecuteTimeInterceptor.class.getName());

    //before the actual handler will be executed
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
            Object handler) throws Exception {

        long startTime = System.currentTimeMillis();
        request.setAttribute("startTime", startTime);

        return true;
    }

    //after the handler is executed
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response,
            Object handler, ModelAndView modelAndView) throws Exception {

        long startTime = (Long) request.getAttribute("startTime");

        long endTime = System.currentTimeMillis();

        long executeTime = endTime - startTime;
        
        logger.log(Level.INFO, "[" + handler + "] executeTime : " + executeTime + "ms");
    }
}
