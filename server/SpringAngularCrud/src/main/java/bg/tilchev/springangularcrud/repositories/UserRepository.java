/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bg.tilchev.springangularcrud.repositories;

import bg.tilchev.springangularcrud.entities.User;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author todor.ilchev
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    
    @Override
    public User saveAndFlush(User user);
    
    @Override
    public User getOne(Long id);
    
    @Override
    public void delete(User user);
    
    @Override
    public List<User> findAll();
}
